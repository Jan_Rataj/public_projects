using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IresoftTask
{
    public partial class Form1 : Form
    {
        private bool stop = false;

        private string FileName = "";

        public Form1()
        {
            InitializeComponent();
        }

        private StringBuilder SBforChangeText { get; set; }

        private string GetDataToFile
        {
            get { return SetDataToFile; }
        }
        private string SetDataToFile
        {
            get { return SetDataToFile; }
            set { SetDataToFile = value; }
        }

        public bool rtrn()
        {
            return true;
        }

        private static readonly object labelLock = new object();

        CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

        private async void button1_Click(object sender, EventArgs e)
        {
            try
            {

                stop = false;
                
                openFileDialog1.ShowDialog();
                
                FileName = openFileDialog1.FileName;
               
                string Data = File.ReadAllText(FileName);

                string[] Parts = Data.Split(' ');

                if (!string.IsNullOrEmpty(Data))
                {

                    StatisticFromText Statistic = new StatisticFromText(Data, Parts);

                    Task<Tuple<int, int, int, int>> getNumbersTask = Task.Run(() => Statistic.GetNumbers(cancellationTokenSource.Token), cancellationTokenSource.Token);

                    Task Monitoring_ = Task.Run(() => PrintObjectValuesWhileTaskRunningAsync(getNumbersTask, Statistic, Parts, cancellationTokenSource.Token));  // 1 metoda menici label

                    await Task.WhenAll(getNumbersTask, Monitoring_);

                    ChangeText ChangeText_ = new ChangeText(Data);

                    Task ChangeTextTask = Task.Run(() => ChangeText_.DeserializeText(cancellationTokenSource.Token), cancellationTokenSource.Token);

                    Task Monitoring = Task.Run(() => PrintObjectValuesWhileTaskRunningAsync_(ChangeTextTask, ChangeText_, Parts, cancellationTokenSource.Token)); // 2 metoda menici label

                    await Task.WhenAll(ChangeTextTask, Monitoring);

                    Tuple<int, int, int, int> Result = getNumbersTask.Result;

                    SBforChangeText = ChangeText_.DeserializeText_;

                    ShowResult(Result, ChangeText_);
                }
                else
                {
                    Storno();
                }

            }
            catch (OperationCanceledException)
            {
            }
            catch (Exception ex)
            {
            }
        }

        //STORNO METODA

        private void button3_Click(object sender, EventArgs e)
        {
            Storno();
            stop = true;
            cancellationTokenSource.Cancel();
            cancellationTokenSource = new CancellationTokenSource();
        }

        //VYPISOVANI % A PROGRESS BARU

        private async Task PrintObjectValuesWhileTaskRunningAsync(Task taskToMonitor, StatisticFromText x, string[] Parts, CancellationToken cancellationToken)
        {
            while (!taskToMonitor.IsCompleted)
            {
                progressBar1.Invoke(new Action(() =>
                {
                    progressBar1.Maximum = Parts.Length * 2;
                    progressBar1.Minimum = 0;
                }));

                double Maximum = Parts.Length * 2;

                double text_percent = (1.0 / Maximum) * (double)x.ActuallyIndexGet;

                int x_ = (int)Math.Round(text_percent * 100);

                lock (labelLock)
                {
                    Loading.Invoke(new Action(() => Loading.Text = x_.ToString() + "%"));
                }

                progressBar1.Invoke(new Action(() => progressBar1.Value = x.ActuallyIndexGet));

            }
            await taskToMonitor;
        }



        private async Task PrintObjectValuesWhileTaskRunningAsync_(Task taskToMonitor, ChangeText x, string[] Parts, CancellationToken cancellation)
        {
            while (!taskToMonitor.IsCompleted)
            {

                progressBar1.Invoke(new Action(() =>
                {
                    progressBar1.Maximum = Parts.Length * 2;
                    progressBar1.Minimum = 0;
                }));

                double Maximum = Parts.Length;

                double text_percent = (1.0 / Maximum) * (double)x.ActuallyIndexGet;

                text_percent = text_percent / 2;

                int PercentToString = (int)Math.Round(text_percent * 100);

                PercentToString += 50;

                lock (labelLock)
                {
                    Loading.Invoke(new Action(() => Loading.Text = PercentToString.ToString() + "%"));
                }

                progressBar1.Invoke(new Action(() => progressBar1.Value = Parts.Length + x.ActuallyIndexGet));
            }


            await taskToMonitor;

            await Task.Delay(1000);
            progressBar1.Invoke(new Action(() => progressBar1.Value = progressBar1.Minimum));
            Loading.Invoke(new Action(() => Loading.Text = "COMPLETED"));
        }

        // ULO�EN� SOUBORU

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string filePath = saveFileDialog1.FileName;
                File.WriteAllText(filePath, SBforChangeText.ToString());
                Info.Text =Environment.NewLine+ "TEXT ULO�EN" + "  "+ DateTime.Now + Info.Text;
            }
        }

      
        //VY�IST�N� DAT 

        public void Storno()
        {
            stop = true;
            
            if (SBforChangeText != null)
            {
                SBforChangeText.Clear();
            }

            string TextForLine = "PO�ET ��DEK:  ";
            string TextForWords = "PO�ET SLOV:  ";
            string TextForSenteces = "PO�ET V�T:  ";
            string TextForCharacters = "PO�ET ZNAKU:  ";

            progressBar1.Value = progressBar1.Minimum;

            LabelLine.Text = TextForLine;
            LabelLetters.Text = TextForWords;
            LabelSenteces.Text = TextForSenteces;
            LabelCharacters.Text = TextForCharacters;
            TextForLetters.Text = "PO�ET P�SMEN:";
            
            Loading.Text = "";
            Capacity.Text = "VELIKOST SOUBORU:";
            Info.Text = Environment.NewLine + "STORNO AKCE" + "  " + DateTime.Now + "   " +"��dn� data k ulo�en�"+"   "+Info.Text;
        }


        private void ShowResult(Tuple<int, int, int, int> Result, ChangeText ChangeText_)
        {
            if (stop == false)
            {
                string TextForLine = "PO�ET ��DEK:  ";
                string TextForWords = "PO�ET SLOV:  ";
                string TextForSenteces = "PO�ET V�T:  ";
                string TextForCharacters = "PO�ET ZNAKU:  ";
                
                LabelLine.Text = TextForLine + Result.Item1.ToString();
                LabelLetters.Text = TextForWords + Result.Item2.ToString();
                LabelSenteces.Text = TextForSenteces + Result.Item3.ToString();
                LabelCharacters.Text = TextForCharacters + Result.Item4.ToString();

                int byteCount = System.Text.Encoding.UTF8.GetByteCount(SBforChangeText.ToString());

                TextForLetters.Text = "PO�ET P�SMEN:" + "  " + ChangeText_.NumOfLettersGet.ToString();
                Capacity.Text = "VELIKOST SOUBORU:" + " " + byteCount + " " + "BYTES";
                
                Info.Text = Environment.NewLine + "TEXT USP̊N� ZM�N�N" + "  " + DateTime.Now+"  " + " SOUBOR: "+ "  " + FileName + "   "+ "P�IPRAV�N K ULO�EN�   "+Info.Text;
            }
            else
            {
                Storno();
            }
        }
    }
}