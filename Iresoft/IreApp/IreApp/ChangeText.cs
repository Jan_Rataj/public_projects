﻿using System.Text;
using System.Threading;

namespace IresoftTask
{
    internal class ChangeText
    {
        private string Text;
        
        private string NewText;
        
        private int ActuallyIndexSet;
       
        private int NumOfLetters;

        public bool storno;

        public StringBuilder DeserializeText_  
        {
            get { return SbForChangeText; }  
        }

        public int ActuallyIndexGet
        {
            get { return ActuallyIndexSet; }
        }

        public int NumOfLettersGet
        {
            get { return NumOfLetters; }
        }

        public ChangeText(string text)
        {
            this.Text = text;
        }

        StringBuilder SbForChangeText = new StringBuilder();
        public  void  DeserializeText(CancellationToken cancellationToken)
        {
            
            string[] Parts = Text.Split(' ');
            
            bool Situtation = true;

            for (int i = 0; i < Parts.Length; i++)
            {

                if (cancellationToken.IsCancellationRequested)
                {
                    storno = true;
                    return;
                }
             
                ActuallyIndexSet = i;

                Situtation = true;

                string CleanItem = Parts[i];
                
                char[] c = CleanItem.ToCharArray();

                for (int j = 0; j < c.Length; j++)
                {
                    if (cancellationToken.IsCancellationRequested)
                    {                     
                        return;
                    }

                    string PartNew = c[j].ToString();
                    char x = c[j];

                    if (x != '.' && x != '!' && x != '?' && x != ',' && x != ':' && x!= '\r' && x != '\n')
                    {
                        if (Situtation == true && char.IsLetter(x) && x != '.' && x != '!' && x != '?' && x != ',' && x != ':')
                        {
                            string GetSet = c[j].ToString();
                            PartNew = GetSet.ToUpperInvariant();
                            Situtation = false;
                        }
                        else if (char.IsUpper(c[j]) && j > 0 && char.IsLetter(x))
                        {
                            string GetSet = c[j].ToString();
                            PartNew = GetSet.ToLowerInvariant();
                        }
                        else if (x == '.' || x == '!' || x == '?' || x == ',' || x == ':')
                        {
                            Situtation = true;
                        }

                        if (x != '.' && x != '!' && x != '?' && x != ',' && x != ':')
                        {
                            NumOfLetters++;
                            string CleanString = PartNew.Replace('Č', 'C').Replace('Ě', 'E').Replace('Š', 'S').Replace('Ř', 'R').Replace('Ž', 'Z').Replace('Ý', 'Y').Replace('Á', 'A').Replace('Í', 'I').Replace('É', 'E').Replace('Ů', 'U').Replace('Ú', 'U').Replace('Ň', 'N').Replace('Ť', 'T').Replace('Ď', 'D').Replace('Ľ', 'L').Replace('Ĺ', 'L').Replace('Ô', 'O').Replace('Ä', 'A').Replace('Ú', 'U').Replace('Ö', 'O').Replace('č', 'c').Replace('ě', 'e').Replace('š', 's').Replace('ř', 'r').Replace('ž', 'z').Replace('ý', 'y').Replace('á', 'a').Replace('í', 'i').Replace('é', 'e').Replace('ů', 'u').Replace('ú', 'u').Replace('ň', 'n').Replace('ť', 't').Replace('ď', 'd').Replace('ľ', 'l').Replace('ĺ', 'l').Replace('ô', 'o').Replace('ä', 'a').Replace('ú', 'u').Replace('ö', 'o');
                            SbForChangeText.Append(CleanString);
                        }

                    } 
                    else
                    {
                        Situtation = true; //
                    }
                }
            }
        
        }

    }
}
