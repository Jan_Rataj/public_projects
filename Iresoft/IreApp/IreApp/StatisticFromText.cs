﻿using System.Threading;

namespace IresoftTask
{
    internal class StatisticFromText
    {
     

        private int ActuallyIndexSet;

        private string Text;

        private string NewText { get; set; }

        private string[] Parts;

        private string _text;

        public string DeserializeText_   
        {
            get { return NewText; }   
        }

      
        public StatisticFromText(string Text, string[] Parts)
        {
            this.Parts = Parts;
            this.Text = Text;
            this._text = Text;
        }
        public int ActuallyIndexGet
        {
            get { return ActuallyIndexSet; }
        }

     
        public  Tuple <int,int ,int, int> GetNumbers(CancellationToken cancellationToken)
        {


            bool NewLineBool = true;

            int NumOfWords = 0;

            int NumOfLine = 1;

            int NumOfSentences = 0;

            int NumOfCharacters = 0;
           
            /*
                 Zde probíhá výpočet slov 
                -----------------------------
            KOD ZKONTROLUJE : POČET VĚT     ZDA DANY STRING NEOBSAHUJE TECČKU A /N /R  V PŘÍPADĚ OBSAHU PŘIDA BOOL NA TRUE A POTE KONTROLUJE NADCHAZEJICI CHAR 
                              POČET ZNAKU
                              POČET SLOV - TAKTÉŽ ZKONTROLUJE ZDA JEDEN STRING NEOBSAHUJE TEČKY 
                              POČET ŘÁDKU - ZDE JE PROBLEM  PŘI KOPÍROVANÍ TEXTU - SPOČÍTA JEN KDYZ MANUALNE PISEME TEXT A POUZIJEME ENTER            
             */
          
            for (int i = 0; i < Parts.Length; i++)
            {
               
                ActuallyIndexSet = i; // PRO TRIDU FORM1 KDE ZPRACOVAVA LOADING 
            
                string Part = Parts[i];

                if (cancellationToken.IsCancellationRequested) // STORNO 
                {
                    Tuple<int, int, int, int> RESULT_NULL = new Tuple<int, int, int, int>(0,0,0,0);
            
                    return RESULT_NULL;
                }

                if (Part != string.Empty && NewLineBool == true)
                {
                    string CleanPart = Part.Replace("\r", "").Replace("\n", "");

                    NumOfSentences += FirstSentences(CleanPart);

                    NewLineBool = false;
                }

                if (Part.Contains(".") || Part.Contains(",") || Part.Contains("!") || Part.Contains("?") || Part.Contains(":"))
                {

                    string Clean = Part.Replace("\r", "").Replace("\n", "");

                    bool UnvalidTextBool = ExceptionUnvalidText(Clean); 

                    if (UnvalidTextBool == true)
                    {
                        int Num = ActionWithUnvalidText(Clean);

                        NumOfSentences = Num + NumOfSentences;

                        bool LastCharacterBool = CheckLastLetter(Clean);

                        if (LastCharacterBool == true)
                        {
                            NewLineBool = true;
                        }
                    }
                    else
                    {
                        NewLineBool = true;
                    }
                }

             

                if (Part != string.Empty)
                {
                    string CleanPart = Part.Replace("\r", "").Replace("\n", "");

                    NumOfCharacters = GetNumOfCharacters(NumOfCharacters, CleanPart.Length);

                    NumOfWords += GetNumOfLetters(Part);
                }

                int Num_ = GetNumOfline(Part);
                
                NumOfLine = Num_ + NumOfLine;
            }


            Tuple<int, int, int, int> result = new Tuple<int, int, int, int>(NumOfLine, NumOfWords, NumOfSentences, NumOfCharacters);

            return result;
        }

        // FUNKCE PRO VYPOČET VĚT 

        public int FirstSentences(string Item)
        {
            int sentencesNum = 0;

            char[] Arr = Item.ToCharArray();

            bool LetterBool = (char.IsLetter(Arr[0]));

            if (LetterBool == true)
            {
                sentencesNum = 1;
            }

            return sentencesNum;
        }


        public bool ExceptionUnvalidText (string Item)
        {
            char[] PartToChar = Item.ToCharArray();
            
            bool LetterBool = false;
           
            bool CharBool = false;

            foreach (char c in PartToChar)
            {
                LetterBool = (char.IsLetter(c));

                if (LetterBool == true)
                {
                    if (CharBool == true)
                    {
                        return true;
                    }
                }

                if (c == '.' || c == '!' || c == '?' || c == ',' || c == ':')
                {
                    CharBool = true;
                }
            }
            return false;
        }


        public int ActionWithUnvalidText(string item)
        {
            char[] arr = item.ToCharArray();
            
            int NumOfSentences = 0;
            
            bool Action = false;
           
            bool Letters = false;

            foreach (char c in arr)
            {
                Letters = (char.IsLetter(c));

                if (Letters == true && Action == true)
                {
                    NumOfSentences++;
                    Action = false;
                }

                if (c == '.' || c == '!' || c == '?' || c == ',' || c == ':')
                {
                    Letters = false;
                    Action = true;
                }
            }
            
            return NumOfSentences;
        }

        public bool CheckLastLetter(string item)
        {
            string clean = item.Replace("\r", "").Replace("\n", "");
     
            char sixthChar = item[clean.Length - 1];
          

            if ((char.IsLetter(sixthChar)))
            {
                return false;
            }
            return true;
        }

        // VÝPOČET SLOV
        public int GetNumOfLetters(string Item)
        {
            char[] charArray = Item.ToCharArray();
            
            int NumOfLetters = 0;
            
            bool BigLetter = false;
            
            bool FirtsLetter = false;

            foreach (char c in charArray)
            {
                if ((char.IsLetter(c)) && FirtsLetter == false || BigLetter == true && (char.IsLetter(c)))
                {
                    NumOfLetters++;
                    FirtsLetter = true;
                    BigLetter = false;
                }
                if (c == '.' || c == '!' || c == '?' || c == ',' || c == ':' || c == '\n' || c == '\r')
                {
                    BigLetter = true;
                }
            }
            return NumOfLetters;
        }

        //VÝPOČET ŘÁDKY      
        public int GetNumOfline(string Parts)
        {
            int NumOfLines = 0;
            
            char[] Part = Parts.ToCharArray();

            foreach (char c_ in Part)
            {
                if (c_ == '\n')
                {
                    NumOfLines++;
                }
            }
            return NumOfLines;
        }


        // PŘIČÍTÁNÍ

        public int GetNumOfSentences(int NumOfSentences)
        {
            NumOfSentences++;
            return NumOfSentences;
        }
        public int GetNumOfCharacters(int NumOfCharacters, int LengthOf)
        {
            int result = NumOfCharacters + LengthOf;
            return result;
        }

    }
}
