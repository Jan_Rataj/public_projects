﻿namespace IresoftTask
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            saveFileDialog1 = new SaveFileDialog();
            openFileDialog1 = new OpenFileDialog();
            button1 = new Button();
            button2 = new Button();
            LabelSenteces = new Label();
            LabelLine = new Label();
            LabelCharacters = new Label();
            LabelLetters = new Label();
            panel1 = new Panel();
            Capacity = new Label();
            TextForLetters = new Label();
            label2 = new Label();
            label1 = new Label();
            button3 = new Button();
            progressBar1 = new ProgressBar();
            Info = new TextBox();
            Loading = new Label();
            panel1.SuspendLayout();
            SuspendLayout();
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // button1
            // 
            button1.Dock = DockStyle.Bottom;
            button1.Location = new Point(0, 536);
            button1.Name = "button1";
            button1.Size = new Size(1252, 40);
            button1.TabIndex = 5;
            button1.Text = "OTEVŘÍT SOUBOR";
            button1.UseVisualStyleBackColor = true;
            button1.Click += button1_Click;
            // 
            // button2
            // 
            button2.Dock = DockStyle.Bottom;
            button2.Location = new Point(0, 489);
            button2.Name = "button2";
            button2.Size = new Size(1252, 47);
            button2.TabIndex = 8;
            button2.Text = "ULOŽIT SOUBOR";
            button2.UseVisualStyleBackColor = true;
            button2.Click += button2_Click_1;
            // 
            // LabelSenteces
            // 
            LabelSenteces.AutoSize = true;
            LabelSenteces.Location = new Point(236, 21);
            LabelSenteces.Name = "LabelSenteces";
            LabelSenteces.Size = new Size(71, 15);
            LabelSenteces.TabIndex = 0;
            LabelSenteces.Text = "POČET VĚT :";
            // 
            // LabelLine
            // 
            LabelLine.AutoSize = true;
            LabelLine.Location = new Point(235, 64);
            LabelLine.Name = "LabelLine";
            LabelLine.Size = new Size(87, 15);
            LabelLine.TabIndex = 3;
            LabelLine.Text = "POČET ŘÁDKŮ:";
            // 
            // LabelCharacters
            // 
            LabelCharacters.AutoSize = true;
            LabelCharacters.Location = new Point(235, 79);
            LabelCharacters.Name = "LabelCharacters";
            LabelCharacters.Size = new Size(88, 15);
            LabelCharacters.TabIndex = 1;
            LabelCharacters.Text = "POČET ZNAKU:";
            // 
            // LabelLetters
            // 
            LabelLetters.AutoSize = true;
            LabelLetters.Location = new Point(235, 36);
            LabelLetters.Name = "LabelLetters";
            LabelLetters.Size = new Size(77, 15);
            LabelLetters.TabIndex = 2;
            LabelLetters.Text = "POČET SLOV:";
            // 
            // panel1
            // 
            panel1.BackColor = Color.FromArgb(0, 64, 64);
            panel1.Controls.Add(Capacity);
            panel1.Controls.Add(TextForLetters);
            panel1.Controls.Add(label2);
            panel1.Controls.Add(label1);
            panel1.Controls.Add(LabelLetters);
            panel1.Controls.Add(LabelCharacters);
            panel1.Controls.Add(LabelLine);
            panel1.Controls.Add(LabelSenteces);
            panel1.Dock = DockStyle.Top;
            panel1.Location = new Point(0, 0);
            panel1.Margin = new Padding(0);
            panel1.Name = "panel1";
            panel1.Size = new Size(1252, 102);
            panel1.TabIndex = 9;
            // 
            // Capacity
            // 
            Capacity.AutoSize = true;
            Capacity.Location = new Point(707, 64);
            Capacity.Name = "Capacity";
            Capacity.Size = new Size(116, 15);
            Capacity.TabIndex = 13;
            Capacity.Text = "VELIKOST SOUBORU:";
            // 
            // TextForLetters
            // 
            TextForLetters.AutoSize = true;
            TextForLetters.Location = new Point(707, 36);
            TextForLetters.Name = "TextForLetters";
            TextForLetters.Size = new Size(91, 15);
            TextForLetters.TabIndex = 12;
            TextForLetters.Text = "POČET PÍSMEN:";
            // 
            // label2
            // 
            label2.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label2.Location = new Point(453, 21);
            label2.Name = "label2";
            label2.Size = new Size(208, 73);
            label2.TabIndex = 11;
            label2.Text = "VÝSTUPNÍ DATA  STATISTIKA:";
            // 
            // label1
            // 
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(3, 21);
            label1.Name = "label1";
            label1.Size = new Size(193, 73);
            label1.TabIndex = 10;
            label1.Text = "VSTUPNÍ DATA  STATISTIKA:";
            // 
            // button3
            // 
            button3.Dock = DockStyle.Bottom;
            button3.Location = new Point(0, 442);
            button3.Name = "button3";
            button3.Size = new Size(1252, 47);
            button3.TabIndex = 11;
            button3.Text = "STORNO";
            button3.UseVisualStyleBackColor = true;
            button3.Click += button3_Click;
            // 
            // progressBar1
            // 
            progressBar1.BackColor = SystemColors.ButtonFace;
            progressBar1.Dock = DockStyle.Top;
            progressBar1.Location = new Point(0, 102);
            progressBar1.Name = "progressBar1";
            progressBar1.Size = new Size(1252, 27);
            progressBar1.TabIndex = 13;
            // 
            // Info
            // 
            Info.Dock = DockStyle.Bottom;
            Info.Location = new Point(0, 184);
            Info.Multiline = true;
            Info.Name = "Info";
            Info.ScrollBars = ScrollBars.Both;
            Info.Size = new Size(1252, 258);
            Info.TabIndex = 14;
            // 
            // Loading
            // 
            Loading.BackColor = SystemColors.Control;
            Loading.Font = new Font("Segoe UI Black", 15.75F, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point);
            Loading.ForeColor = SystemColors.ActiveCaptionText;
            Loading.Location = new Point(453, 132);
            Loading.Name = "Loading";
            Loading.Size = new Size(282, 49);
            Loading.TabIndex = 13;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1252, 576);
            Controls.Add(Loading);
            Controls.Add(Info);
            Controls.Add(progressBar1);
            Controls.Add(button3);
            Controls.Add(panel1);
            Controls.Add(button2);
            Controls.Add(button1);
            Name = "Form1";
            Text = "Form1";
            panel1.ResumeLayout(false);
            panel1.PerformLayout();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private SaveFileDialog saveFileDialog1;
        private OpenFileDialog openFileDialog1;
        private Button button1;
        private Button button2;
        private Label LabelSenteces;
        private Label LabelLine;
        private Label LabelCharacters;
        private Label LabelLetters;
        private Panel panel1;
        private Label label1;
        private Button button3;
        private ProgressBar progressBar1;
        private TextBox Info;
        private Label TextForLetters;
        private Label label2;
        private Label Loading;
        private Label Capacity;
    }
}