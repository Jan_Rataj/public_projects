﻿using piškvorky;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace piškvorkyORG
{
    internal class Select_Move
    {

        public int GetMove
        {
            get { return GetMove_Prvt; }
        }

        private int GetMove_Prvt;
        
       
  

        List<int> O_USER = new List<int>();
        List<int> OO_USER = new List<int>();
        List<int> XOOO_User = new List<int>();
        List<int> OOO_User = new List<int>();  // človek  
        List<int> OOOO_User = new List<int>();
       
      
      
        List<int> X_PC = new List<int>();
        List<int> XX_PC = new List<int>();
        List<int> OXXX_PC = new List<int>();
        List<int> XXX_PC = new List<int>();
        List<int> XXXX_PC = new List<int>();

        public void AddingPointToList(int[,]Game_Field)
        {
            int INDEXofMOVE = 0;
            int controlerX = 0;
            int controlerY = -1;



            for (int o = 0; o < 225; o++)
            {
                INDEXofMOVE++;

                if (controlerY == 14)
                {
                    controlerY = 0;
                    controlerX++;
                }
                else
                {
                    controlerY++;
                }

                if (Game_Field[controlerX, controlerY] == 101)
                {
                    O_USER.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 151)
                {
                    OO_USER.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 201)
                {
                    X_PC.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 251)
                {
                    XX_PC.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 5)
                {
                    XOOO_User.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 7)
                {
                    OOO_User.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 10)
                {
                    OOOO_User.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 11)
                {
                    OXXX_PC.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 14)
                {
                    XXX_PC.Add(INDEXofMOVE);
                }
                if (Game_Field[controlerX, controlerY] == 20)
                {
                    XXXX_PC.Add(INDEXofMOVE);
                }
            }
        }
        public void SelectBestMove()
        {

         
            if (OOOO_User.Count > 0)      // ooooo
            {
                GetMove_Prvt = OOOO_User[0];
            }

            else if (XXXX_PC.Count > 0)           //xxxx
            {
                GetMove_Prvt = XXXX_PC[0];
            }
           

            else if (OOO_User.Count > 0)       // xxx
            {
                GetMove_Prvt = OOO_User[0];
            }

            else if (XXX_PC.Count > 0)     // ooo
            {
                GetMove_Prvt = XXX_PC[0];
            }

            else if (XOOO_User.Count > 0)   //oxxx
            {
               GetMove_Prvt = XOOO_User[0];
            }

            else if (OXXX_PC.Count > 0)   // 
            {
                GetMove_Prvt = OXXX_PC[0];
            }



            else if (XX_PC.Count > 0)
            {
                GetMove_Prvt = XX_PC[0];
            }

            else if (X_PC.Count > 0)
            {
                GetMove_Prvt = X_PC[0];
            }

            else if (OO_USER.Count > 0)
            {
                GetMove_Prvt = OO_USER[0];
            }

            else if (O_USER.Count > 0)
            {
                GetMove_Prvt = O_USER[0];
            }
        }
    
    }
}
