﻿using Microsoft.VisualBasic;
using piškvorkyORG;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace piškvorky
{
    public partial class Form1 : Form
    {
        public int x = 5;
        public Form1()
        {
            InitializeComponent();
        }
       
        int ScorePlayer1 = 0;
       
        int ScorePlayer2 = 0;
      
        int controlX = 0;     

        int controlY = -1;

        int WIN ;
       
        string NameOfPlayer;
        
        string NameOfPlayer2;
               
        public int BorderField = 15; 
       
        byte  GameSitutation = 0; // game life
       
        public int GamingPlayer = 0;  //player switch
       
        public int Control_While = 0;
       
        public int NewlineINT = 15;
       
        List<int> numbers = new List<int>();
       
        public int[,] Gaming_field = new int[15, 15];

        int lastx = 0;
       
        int lasty = 0;


     
        private void HRA(object sender, EventArgs e)
        {
            if (GameSitutation == 1)
            {
               
                Button button = (Button)sender;    // ziskame nazev button ktery byl kliknuty a podle toho hledame v poli

                int POSITION = ClassofFunction.RankButton(button.Name);


                if (numbers.Contains(POSITION) == false)   // pokud dane misto nebylo jeste vyuzito provede se kod 
                {
                    
                     numbers.Add(POSITION);
                    
                     ClassofFunction.Addposition(POSITION, out controlX, out controlY);


                    if (GamingPlayer == 1)
                    {
                   
                        button.Text = "X       ";           
                        Gaming_field[controlX, controlY] = 1;
                        WIN = ClassofFunction.Check_Win(controlX, controlY,Gaming_field, GamingPlayer);
                       
                        if (WIN == 1)
                        {
                            ScorePlayer1 = ScorePlayer1 + 1;
                            score1.Text = Convert.ToString(ScorePlayer1);
                            string Message_Win = Convert.ToString("VYHRAL ! :" + "   " + NameOfPlayer);
                            MessageBox.Show(Message_Win);
                            newgame();
                        }
                        if (WIN != 1)
                        {
                            GamingPlayer = 2;
                            hracbuttonhraje.Text = NameOfPlayer2;
                        }


                    }
                    else if (GamingPlayer == 2)
                    {            
                        button.Text = "O       ";
                        Gaming_field[controlX, controlY] = 2;
                        WIN = ClassofFunction.Check_Win(controlX, controlY,Gaming_field,GamingPlayer);

                        if (WIN == 1)
                        {
                            ScorePlayer2 = ScorePlayer2 + 1;
                            score2.Text = Convert.ToString(ScorePlayer2);
                            string vyherce = Convert.ToString("VYHRAL ! :" + "     " + NameOfPlayer2);
                            MessageBox.Show(vyherce);
                            newgame();
                        }

                        if (WIN != 1)
                        {
                            GamingPlayer = 1;
                            hracbuttonhraje.Text = NameOfPlayer;
                        }
                    }

                }
            }

            if (GameSitutation == 3)   // zde je USER VS PC 
            {

                if (GamingPlayer == 1)
                {
                    Button button = (Button)sender;

                    int pozice = ClassofFunction.RankButton(button.Name);
                    
                    if (numbers.Contains(pozice) == false)
                    { 

                            numbers.Add(pozice);

                            ClassofFunction.Addposition(pozice, out controlX, out controlY);
                         
                            button.Text = "X       ";
                           
                            Gaming_field[controlX, controlY] = 1;
                            
                            WIN =  ClassofFunction.Check_Win(controlX, controlY,Gaming_field,GamingPlayer);
                            
                            GamingPlayer = 2;
                           
                            if (WIN == 1)
                            {
                                ScorePlayer1 = ScorePlayer1 + 1;
                                score1.Text = Convert.ToString(ScorePlayer1);
                                string vyherce = Convert.ToString("VYHRAL ! :" + "   " + NameOfPlayer);
                                MessageBox.Show(vyherce);                               
                                newgame();
                                GamingPlayer = 1;
                            }
                    }
                }

                if (GamingPlayer == 2)
                {
                                            
                    Control_While = 0;      
               
                    int controlX_ = 0;   

                    int ControlY_ = -1;   

                    Defensive Defensive_ = new Defensive(); // hleda souperovo tahy
                    Defensive_.defensive(Gaming_field);
                   
                    offensive Ofensive_ = new offensive();  // hleda sve tahy 
                    Ofensive_.offe(Gaming_field);
                   
                    Select_Move tah = new Select_Move();           // vypise do listu
                    tah.AddingPointToList(Gaming_field);

                    tah.SelectBestMove();             // vybere nejleps tah
                    int Move  = tah.GetMove;
                    numbers.Add(Move);

                    ClassofFunction.Addposition(Move, out controlX_, out ControlY_); // vypocte z move controly                  
                   
                    Gaming_field[controlX_, ControlY_] = 2;  // prida do pole 
                  
                    string buttonName = ClassofFunction.ASSIGN_BUTTON(controlX_, ControlY_);
                  
                    Button buttonToEdit = this.Controls.Find(buttonName, true).FirstOrDefault() as Button;
                  
                    buttonToEdit.Text = "O       ";   // graficky vypis 

                    WIN = ClassofFunction.Check_Win(controlX_, ControlY_,Gaming_field,GamingPlayer);

                    if (WIN == 1)
                    {
                        ScorePlayer2 = ScorePlayer2 + 1;
                        score2.Text = Convert.ToString(ScorePlayer2);

                        string vyherce = Convert.ToString("VYHRAL ! :" + "   " + NameOfPlayer2);
                        MessageBox.Show(vyherce);
                        newgame();
                    }
               
                    GamingPlayer = 1;
                    Control_While = 0;
               
                    controlX_ = 0;
                    ControlY_ = -1;
                             
                    ClassofFunction.CleanDataofPosition(controlX_, ControlY_, ref Gaming_field); // vycisti pole od vseho krom 1 a 2 
                }
            }
        }

    
           
        void newgame()
        {
            numbers.Clear();
            WIN = 0;
            button226.Text = "NOVÁ HRA";
            int FOURTEEN = 14;
            int ControlX = 0;
            int ControlY = 0;

            for (x = 0; x < 226; x++)
            {
                var button = this.Controls.Find("button" + x.ToString(), true).FirstOrDefault() as Button;

                if (button != null)
                {
                    button.Text = "";
                }
            }

            for (x = 0; x < 225; x++)
            {

                Gaming_field[ControlX, ControlY] = 0;

                if (FOURTEEN == ControlY)
                {
                    ControlY = 0;

                    if (ControlY == 14 && ControlX == 14)
                    {
                        ControlY = 0;
                        ControlX = 0;
                    }
                    else
                    {
                        ControlX = ControlX + 1;
                    }
                }
                else
                {
                    ControlY++;
                }
            }
        }
   
        private void button229_Click(object sender, EventArgs e)
        {

            newgame();
            
            GameSitutation = 1;
            
            string input = Interaction.InputBox("HRAČ1", "Název okna", "Zadejte přezdívku", -1, -1);

            string input2 = Interaction.InputBox("HRAČ2", "Název okna", "Zadejte přezdívk", -1, -1);

            button226.Text = "UKONČIT HRU";

            label1.Text = "HRÁČ:" + "  " + input;

            label2.Text = "HRÁČ:" + "  " + input2;

            NameOfPlayer = input;
            NameOfPlayer2 = input2;

            int min = 1, max = 2, random_num;

            Random rand = new Random();

            random_num = rand.Next(min, max);

            GamingPlayer = random_num;

            score1.Text = "0";
            score2.Text = "0";

            ScorePlayer1 = 0;
            ScorePlayer2 = 0;

        }

        private void button226_Click_1(object sender, EventArgs e)
        {
            if (GameSitutation == 1 || GameSitutation == 3)
            {

                string question = Interaction.InputBox("Chcete začít novou serii?", "JAN RATAJ PIŠKVORKY ","odpovez ano/ne", -1, -1);
                if(question == "ano")
                { GameSitutation = 0;}

            }

            if (GameSitutation == 0)
            {

                newgame();

                string input = Interaction.InputBox("HRAČ1", "Název okna", "Zadejte přezdívku", -1, -1);

                string input2 = Interaction.InputBox("HRAČ2", "Název okna", "Zadejte přezdívku", -1, -1);


                if (input2 == "robot")
                {
                    GameSitutation = 3;
                    GamingPlayer = 1;

                    label1.Text = "HRÁČ:" + "  " + input;

                    label2.Text = "HRÁČ:" + "  " + input2;

                    button226.Text = "NOVÁ HRA";

                    NameOfPlayer = input;

                    NameOfPlayer2 = input2;

                }
                else
                {
                    button226.Text = "NOVÁ HRA";

                    label1.Text = "HRÁČ:" + "  " + input;

                    label2.Text = "HRÁČ:" + "  " + input2;

                    NameOfPlayer = input;

                    NameOfPlayer2 = input2;

                    GamingPlayer = 1;

                    GameSitutation = 1;
                }

                if (GamingPlayer == 1)
                {
                    hracbuttonhraje.Text = input;
                }
                else
                {
                    hracbuttonhraje.Text = input2;
                }
            }


            else if (GameSitutation == 1 || GameSitutation == 3)
            {
                newgame();
            }
        }
    }
}

