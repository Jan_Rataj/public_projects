﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Tab;
using System.Windows.Forms;

namespace piškvorkyORG
{
    internal class Defensive
    {
        public void defensive(int[,] Game_Field)
        {  
            int Control_Y = -1;
            int Control_X = 0;

            for (int o = 0; o < 225; o++)
            {

                if (Control_Y != 14)
                {
                    Control_Y = Control_Y + 1;
                }

                else if (Control_Y == 14)
                {
                    Control_Y = 0;
                    Control_X = Control_X + 1;
                }

                if (Game_Field[Control_X, Control_Y] == 1)
                {

                    byte FreePosition_minus = 0;                   

                    byte REFRESH_WORKING_DATA = 0;

                    byte CHANGE_DIRECTION = 0;

                    byte OFF_ON = 0;  

                    byte AXIS = 0;  

                    byte POINTS = 0;  

                    byte FreePosition_plus = 0; 

                    int x = Control_X;

                    int y = Control_Y; 

                    int x2 = Control_X; 

                    int y2 = Control_Y; 

                    List<int> POINT_PLUS = new List<int>();   

                    List<int> POINT_MINUS = new List<int>();

                    while (OFF_ON == 0)
                    {
                        if (AXIS == 0)
                        {
                            if (CHANGE_DIRECTION == 0)
                            {

                                if (x != 14)
                                {
                                    x++;
                                }

                                else if (x == 14)
                                {
                                    x = Control_X;
                                    y = Control_Y;
                                    CHANGE_DIRECTION = 1;
                                }
                            }

                            if (CHANGE_DIRECTION == 1)
                            {

                                if (x != 0)
                                {
                                    x--;
                                }

                                else if (x == 0)
                                {
                                    FreePosition_minus = 0;
                                    POINTS = 0;
                                    FreePosition_plus = 0;
                                    POINT_MINUS.Clear();
                                    POINT_PLUS.Clear();
                                    REFRESH_WORKING_DATA = 1;
                                    AXIS++;
                                }
                            }
                        }

                        else if (AXIS == 1)
                        {

                            if (CHANGE_DIRECTION == 0)
                            {

                                if (y != 14)
                                {
                                    y++;
                                }

                                else if (y == 14)
                                {
                                    x = Control_X;
                                    y = Control_Y;
                                    CHANGE_DIRECTION = 1;
                                }
                            }

                            if (CHANGE_DIRECTION == 1)
                            {
                                if (y != 0)
                                {
                                    y--;
                                }

                                else if (y == 0)
                                {
                                    FreePosition_minus = 0;
                                    POINTS = 0;
                                    FreePosition_plus = 0;
                                    POINT_MINUS.Clear();
                                    POINT_PLUS.Clear();
                                    REFRESH_WORKING_DATA = 1;
                                    AXIS++;
                                }
                            }
                        }

                        else if (AXIS == 2)
                        {
                            if (CHANGE_DIRECTION == 0)
                            {

                                if (y != 14 && x != 14)
                                {
                                    x++;
                                    y++;
                                }

                                else if (x == 14 || y == 14)
                                {

                                    CHANGE_DIRECTION = 1;
                                    x = Control_X;
                                    y = Control_Y;
                                }
                            }

                            if (CHANGE_DIRECTION == 1)
                            {
                                if (y != 0 && x != 0)
                                {
                                    x--;
                                    y--;
                                }
                                else if (x == 0 || y == 0)
                                {
                                    FreePosition_minus = 0;
                                    FreePosition_plus = 0;
                                    POINTS = 0;
                                    POINT_MINUS.Clear();
                                    POINT_PLUS.Clear();
                                    REFRESH_WORKING_DATA = 1;
                                    AXIS++;
                                }
                            }
                        }

                        else if (AXIS == 3)
                        {

                            if (CHANGE_DIRECTION == 0)
                            {
                                if (y != 0 && x != 14)
                                {
                                    x++;
                                    y--;
                                }

                                else if (CHANGE_DIRECTION == 0 && x == 14 || y == 0)
                                {
                                    CHANGE_DIRECTION = 1;
                                    x = Control_X;
                                    y = Control_Y;
                                }

                            }

                            if (CHANGE_DIRECTION == 1)
                            {
                                if (CHANGE_DIRECTION == 1 && y != 14 && x != 0)
                                {
                                    x--;
                                    y++;
                                }
                                else if (x == 0 || y == 14 && CHANGE_DIRECTION == 1)
                                {
                                    POINTS = 0;
                                    FreePosition_plus = 0;
                                    POINT_PLUS.Clear();
                                    POINT_MINUS.Clear();
                                    OFF_ON = 1;
                                    REFRESH_WORKING_DATA = 1;
                                }
                            }

                        }

                  
                        if (REFRESH_WORKING_DATA != 1)
                        {
                            if (Game_Field[x, y] == 2)
                            {
                                if (CHANGE_DIRECTION == 0)
                                {
                                    CHANGE_DIRECTION = 1;
                                    x = Control_X;
                                    y = Control_Y;
                                }
                                else if (CHANGE_DIRECTION == 1)
                                {                                                     
                                    if (AXIS == 3)
                                    {
                                        REFRESH_WORKING_DATA = 1;
                                    }
                                    else
                                    {
                                        CHANGE_DIRECTION = 0;
                                        FreePosition_minus = 0;
                                        POINTS = 0;
                                        FreePosition_plus = 0;
                                        POINT_MINUS.Clear();
                                        POINT_PLUS.Clear();
                                        REFRESH_WORKING_DATA = 1;
                                        AXIS++;
                                    }

                                }
                            }

                            else if (Game_Field[x, y] != 2 && Game_Field[x, y] != 1)
                            {
                                FreePosition_plus++;
                              
                                if (CHANGE_DIRECTION == 0)
                                {
                                    POINT_PLUS.Add(FreePosition_plus);
                                }
                                else if (CHANGE_DIRECTION == 1)
                                {
                                    FreePosition_minus++;
                                    POINT_MINUS.Add(FreePosition_minus);
                                }
                            }

                            else if (Game_Field[x, y] == 1 && CHANGE_DIRECTION != 1)
                            {
                                POINTS++;
                                FreePosition_plus++;
                            }

                        }

                        if (REFRESH_WORKING_DATA == 1 && CHANGE_DIRECTION == 1)
                        {
                            OFF_ON = 1; 
                            CHANGE_DIRECTION = 0;
                            x = Control_X;
                            y = Control_Y;     
                            REFRESH_WORKING_DATA = 0;
                        }

                        else if (REFRESH_WORKING_DATA == 1)
                        {
                            x = Control_X;
                            y = Control_Y; 
                            REFRESH_WORKING_DATA = 0;
                        }


                        
                        else if (FreePosition_plus == 4)
                        {


                            y = Control_Y;
                            x = Control_X;
                            FreePosition_plus = 0;
                            REFRESH_WORKING_DATA = 1;
                            x2 = Control_X;
                            y2 = Control_Y;

                            if (AXIS == 0)
                            {
                                if (x2 != 0)
                                {
                                    x2--;
                                }
                                if (POINT_PLUS.Count > 0)
                                {
                                    int move = POINT_PLUS[0];
                                    x = x + move;
                                }

                                else if (POINT_MINUS.Count > 0)
                                {
                                    int move = POINT_MINUS[0];
                                    x = x - move;
                                }



                            }
                            if (AXIS == 1)
                            {
                                if (y2 != 0)
                                {
                                    y2--;
                                }

                                if (POINT_PLUS.Count > 0)
                                {
                                    int move = POINT_PLUS[0];
                                    y = y + move;

                                }

                                else if (POINT_MINUS.Count > 0)
                                {
                                    int move = POINT_MINUS[0];
                                    y = y - move;
                                }


                            }
                            if (AXIS == 2)
                            {
                                if (x2 != 0 && y2 != 0)
                                {
                                    x2--;
                                    y2--;
                                }

                                if (POINT_PLUS.Count > 0)
                                {
                                    int move = POINT_PLUS[0];
                                    y = y + move;
                                    x = x + move;
                                }

                                else if (POINT_MINUS.Count > 0)
                                {
                                    int move = POINT_MINUS[0];
                                    y = y - move;
                                    x = x - move;
                                }



                            }

                            if (AXIS == 3)
                            {
                                OFF_ON = 1;

                                if (x2 != 0 && y2 != 14)
                                {
                                    x2--;
                                    y2++;
                                }


                                if (POINT_PLUS.Count > 0)
                                {
                                    y = Control_Y;
                                    x = Control_X;
                                    int move = POINT_PLUS[0];

                                    y = y - move;
                                    x = x + move;
                                }

                                else if (POINT_MINUS.Count > 0)
                                {
                                    int move = POINT_MINUS[0];
                                    y = y + move;
                                    x = x - move;
                                }

                                OFF_ON = 1;
                            }





                            if (POINTS == 3)
                            {

                                if (Game_Field[x, y] != 1 && Game_Field[x, y] != 2)
                                {
                                    Game_Field[x, y] = 20;
                                }

                            }




                            if (POINTS == 2)
                            {
                                if (POINT_PLUS.Count > 1)
                                {

                                    if (POINT_PLUS[0] == 3 && POINT_PLUS[1] == 4)
                                    {

                                        if (AXIS == 0 && Control_X != 0 || AXIS == 1 && Control_Y != 0 || AXIS == 2 && Control_Y != 0 && Control_X != 0 || AXIS == 3 && Control_Y != 0 && Control_X != 0)
                                        {
                                            if (Game_Field[x2, y2] != 1 && Game_Field[x2, y2] != 2)
                                            {

                                                if (Game_Field[x2, y2] != 10 && Game_Field[x, y] != 10 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2)
                                                {

                                                    Game_Field[x, y] = 14;
                                                }

                                            }
                                            else
                                            {

                                                if (Game_Field[x, y] != 10 && Game_Field[x, y] != 7 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 14)
                                                {
                                                    Game_Field[x, y] = 11;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            if (Game_Field[x, y] != 10 && Game_Field[x, y] != 7 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 14)
                                            {
                                                Game_Field[x, y] = 11;
                                            }

                                        }

                                    }
                                    else if (POINT_PLUS[0] == 1 && POINT_PLUS[1] == 4)
                                    {
                                        if (Game_Field[x2, y2] != 10 && Game_Field[x, y] != 10 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2)
                                        {

                                            Game_Field[x, y] = 14;
                                        }
                                    }
                                    else if (POINT_PLUS[0] == 2 && POINT_PLUS[1] == 4)
                                    {


                                        if (Game_Field[x2, y2] != 10 && Game_Field[x, y] != 10 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2)
                                        {

                                            Game_Field[x, y] = 14;
                                        }


                                    }
                                }

                                if (Game_Field[x, y] != 10 && Game_Field[x, y] != 7 && Game_Field[x, y] != 1 && Game_Field[x, y] != 2 && Game_Field[x, y] != 20 && Game_Field[x2, y2] != 20 && Game_Field[x, y] != 14)
                                {
                                    Game_Field[x, y] = 11;
                                }

                            }





                            if (POINTS == 1)
                            {
                                if (Game_Field[x, y] != 10 && Game_Field[x, y] != 2 && Game_Field[x, y] != 1 && Game_Field[x, y] != 5 && Game_Field[x, y] != 7 && Game_Field[x, y] != 14 && Game_Field[x, y] != 20 && Game_Field[x, y] != 11)
                                {

                                    Game_Field[x, y] = 251;


                                }
                            }

                            if (POINTS == 0)
                            {

                                if (Game_Field[x, y] != 10 && Game_Field[x, y] != 2 && Game_Field[x, y] != 1 && Game_Field[x, y] != 5 && Game_Field[x, y] != 7 && Game_Field[x, y] != 14 && Game_Field[x, y] != 20 && Game_Field[x, y] != 11 && Game_Field[x, y] != 251)
                                {

                                    Game_Field[x, y] = 201;


                                }


                            }

                            FreePosition_minus = 0;
                            x = Control_X;
                            y = Control_Y;
                            REFRESH_WORKING_DATA = 0;
                            CHANGE_DIRECTION = 0;
                            FreePosition_plus = 0;
                            POINTS = 0;
                            AXIS++;
                            POINT_PLUS.Clear();
                            POINT_MINUS.Clear();
                        }
                    }
                }
            }
        }    
    }
}
