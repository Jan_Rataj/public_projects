﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIREMNISOFTWARE
{
    public partial class Create : Form
    {

        private int x = 0;

        public Create()
        {
            InitializeComponent();
        }
     
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                //zpracuje input od uzivatele odstrani mezery 

                string Id = textBox1.Text.Trim();
               
                string UserinpurId = Id.Replace(" ", "");
                
                if (string.IsNullOrWhiteSpace(Id))
                {
                    throw new ArgumentException("Jedno nebo více údajů nejsou vyplněné!");
                }
               
                string Code = textBox2.Text.Trim();
                
                string UserinputCode = Code.Replace(" ", "");
                
                if (string.IsNullOrWhiteSpace(Code))
                {
                    throw new ArgumentException("Jedno nebo více údajů nejsou vyplněné!");
                }
                
                
                string Code_Repeat = textBox3.Text.Trim();
                
                string UserinputRepeat= Code_Repeat.Replace(" ", "");
                
                if (string.IsNullOrWhiteSpace(Code))
                {
                    throw new ArgumentException("Jedno nebo více údajů nejsou vyplněné!");
                }
                
                string Companyname = textBox4.Text.Trim();
                
                string UserinputComapnyname = Companyname.Replace(" ", "");
                
                if (string.IsNullOrWhiteSpace(Companyname))
                {
                    throw new ArgumentException("Jedno nebo více údajů nejsou vyplněné!");
                }
        
                Registration cntrl = new Registration (UserinpurId,UserinputCode,UserinputRepeat,UserinputComapnyname);
                
                if(cntrl.callback_rtrn==1)
                {
                    x = 1;
                    Form1 frm = new Form1();
                    frm.WindowState = FormWindowState.Maximized;
                    frm.Show();
                    this.Close();
                   
                }
                else if (cntrl.callback_rtrn==2)
                {
                    MessageBox.Show("Uživatel existuje!");       
                } 
                
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR" + Environment.NewLine + ex.Message);
            }
        }

        private void Create_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
