﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIREMNISOFTWARE
{
    public partial class AddingItems : Form
    {

   
        private string Id { get; set; }
        private int x = 0;
        public AddingItems(string Id)
        {

            this.Id = Id;


            InitializeComponent();
            addToComboBox();

          
        }

        void addToComboBox()
        {
            try
            {
                Work_withdata object_add = new Work_withdata("", "", "", "", Id, 2, "", "");

                if (object_add.Infrastrukturaobject_get.Productname!=null || object_add.Infrastrukturaobject_get.TypeofProduct != null || object_add.Infrastrukturaobject_get.Director != null)
                {
                    foreach (var ob in object_add.Infrastrukturaobject_get.Productname)
                    {
                        comboBox1.Items.Add(ob);
                    }
                    foreach (var ob in object_add.Infrastrukturaobject_get.TypeofProduct)
                    {
                        comboBox2.Items.Add(ob);
                    }
                    foreach (var ob in object_add.Infrastrukturaobject_get.Director)
                    {
                        comboBox3.Items.Add(ob);
                    }
                }
                else 
                {
                    listBox1.Items.Add("Nedostatečné data pro přidaní produktů" + "  " + DateTime.Now + "    " + "FIREMNÍ SOFTWARE");
                }            
            }
            catch
            {
                listBox1.Items.Add("Nedostatečné data pro přidaní produktů" + "  " + DateTime.Now + "    " + "FIREMNÍ SOFTWARE");
            }
        }     
        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text=="" || comboBox2.Text=="" || comboBox3.Text=="" || comboBox4.Text =="" || comboBox7.Text=="")
            {
                MessageBox.Show("Některé pole je prázdné");
            }
            else
            {
                try
                {
                    
                    Work_withdata Addto_Storage = new Work_withdata(comboBox1.Text.Trim(), comboBox2.Text.Trim(), comboBox3.Text.Trim(),comboBox4.Text.Trim(), Id, 4,comboBox7.Text.Trim(),"");

                    string datumPridani = "Datum přidání: " + DateTime.Now.ToString();

                    string space = "   ";

                    string output = "Název produktu: " + "   "+ comboBox1.Text + "\n" + space
                                   + "Typ produktu: " + space + comboBox2.Text+ "\n" + space
                                   + "Dodavatel: " + space+ comboBox3.Text + "\n" + space
                                   + "Cena: " + space
                                   +comboBox4.Text + "\n" + space
                                    + "Kvantita: " +space + comboBox7.Text + "\n" + space
                                   + datumPridani;

                    listBox1.Items.Add(output);
                }
                catch (OverflowException ex)
                {
                    MessageBox.Show("Došlo k přetečení datového typu long: {0}", ex.Message);
                }
                catch
                {
                    MessageBox.Show("Unvalid");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            x = 1;
            Timeline newForm = new Timeline(Id);
            newForm.Show();
            this.Close();

        }

        private void AddingItems_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
