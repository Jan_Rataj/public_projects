﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing.Text;
using System.Collections;
using System.Text.Json;
using System.Xml.Linq;
using System.Security.Cryptography.X509Certificates;

namespace FIREMNISOFTWARE
{
    public class Registration
    {
        private int rtrn_;
       
        private string Id_prvt;
      
        private string CompanyName_prvt;
      
        private string Code_prvt;

        public int callback_rtrn
        {
            get { return rtrn_; }          
        }

        public Registration (string ID, string Code, string Code_repeat, string Company_Name)
        {
            if (Code == Code_repeat)
            {
                this.Code_prvt = Code.Trim();
                
                this.Id_prvt = ID.Trim();
               
                this.CompanyName_prvt = Company_Name;
                 
                Reg(ID, Code, Company_Name);
            }
            else
            {
                MessageBox.Show("Kody se neshodují");
            }
        }

        private void Reg(string ID, string Code, string Company_Name)
        {

            string path = @"C:\" + Id_prvt;  // cesta k souboru       

            string filePathObj = Path.Combine(path, "myObjectList.json");  // cesta k slozce adresatu


            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);         // vytvori se pokud neexistuje soubor 


                if (!File.Exists(filePathObj))
                {

                    Constructor_forUserData.propertie_ on = new Constructor_forUserData.propertie_
                    {
                        Code_ = Code_prvt,
                        Company_Name_ = CompanyName_prvt,
                        ID_ = Id_prvt
                    };


                    string jssonString = JsonSerializer.Serialize(on);

                    File.WriteAllText(filePathObj, jssonString);

                    rtrn_ = 1;

                    MessageBox.Show("Registrace proběhla uspěšně!");
                }
            }
            else
            {
                rtrn_ = 2;
            }
        }
        
    }
    
}


