﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIREMNISOFTWARE
{
     
    public class Log_
    {
        
        private int return_logorfalse;     
        
        private string Code;
        
        private string Id;

        public int Callback_rtnr
        { get { return this.return_logorfalse; } }


        public Log_( string Id_,string Code_)
        {
            this.Code = Code_;
            
            this.Id = Id_;
          
            Check();
        }

       
       
        private void Check()
        {
            string path = @"C:\" + Id.Trim();  // cesta k adresari

            string filePathObj = Path.Combine(path, "myObjectList.json");  //cesta k dane sloze souboru (Login data) 

            if (Directory.Exists(path))
            {      
                    try
                    {
                        string jsonString= File.ReadAllText(filePathObj);   // data Přetahle formatu json
                        
                         Constructor_forUserData.propertie_ obj = JsonSerializer.Deserialize<Constructor_forUserData.propertie_>(jsonString); // Deserializace dat do objectu
                        
                        if(obj.Code_==Code)  
                        {
                            MessageBox.Show("Přihlášeno" + Environment.NewLine + "Vítejte" +" "+ obj.ID_);
                          
                            return_logorfalse =1;
                          
                        }
                        else
                        {
                            MessageBox.Show("Nesprávné heslo");
                        }
                    }

                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR" + Environment.NewLine + ex.Message);
                    }
                
            }
            else
            {
                MessageBox.Show("Id neexistuje");
            }
        }

     

    }
  
}
