﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;

namespace FIREMNISOFTWARE
{
    public class Constructor_forUserData
    {
        public class propertie_
        {
            //Zakladni data o firme
            public string ID_ { get; set; }
            public string Code_ { get; set; }
            public string Company_Name_ { get; set; }
        }

        public class ProductInfo
        {
             // Pro infrastrukturu pro jednodussi zadavani a safe vstupu
            
            public string ProductName { get; set; }
            public string TypeOfProduct { get; set; }
            public string Director { get; set; }          
        }

        public class AddTo_Storage
        {      
            //Konstruktor pro zadani produktu 

            public string ProductName { get; set; }
            public string TypeOfProduct { get; set; }
            public string Director { get; set; }
            public string Price { get; set; }
            public string Quantity { get; set; }

            public string set { get; set; }
            public string Sec { get; set; }
            public string Minute { get; set; }
            public string Hour { get; set; }
            public string Day { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
            public string Identifikator { get; set; }
            public string finalPrice { get; set;}
        }
        public class Infrastructuredata
        {

            //nahrani viz nazev , obsahuje productInfo data viz vyse 
            public List<string> Productname { get; set; }
            public List <string> TypeofProduct { get; set; }
            public List <string>  Director { get; set;}

            public Infrastructuredata()
            {
                Productname = new List<string>();
                TypeofProduct = new List<string>();
                Director = new List<string>();
            }

        }
    }
}

    

