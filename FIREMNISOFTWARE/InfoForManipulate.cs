﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace FIREMNISOFTWARE
{
    public partial class InfoForManipulate : Form
    {

        private string Id;
        private int x = 0;

        public InfoForManipulate(string Id)
        {
            this.Id = Id;

            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            x = 1;
            Timeline objectTimeline = new Timeline(Id);
            objectTimeline.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(textBox1.Text!="" || textBox2.Text!="" || textBox3.Text!="")
            {
                string Product_name = textBox1.Text.Trim();
                string Typeof_product = textBox2.Text.Trim();
                string Director = textBox3.Text.Trim();
                Work_withdata object_wInfo = new Work_withdata(Product_name, Typeof_product ,Director,"",Id,3,"","");
            }
        }

        private void InfoForManipulate_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
