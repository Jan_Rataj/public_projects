﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FIREMNISOFTWARE.sort;
using System.Windows.Forms.DataVisualization.Charting;
using System.Numerics;
using System.Net.NetworkInformation;

namespace FIREMNISOFTWARE
{
    public partial class Analyseform : Form
    {
        private string ID_;
        private int x = 0;
        public Analyseform(string _ID)
        {

            this.ID_ = _ID;
            InitializeComponent();
            Openform();
        
        }

        private void button1_Click(object sender, EventArgs e)
        {

            listBox1.Items.Clear();
             
            string productname_ =  comboBox3.Text;
            
            string typeofproduct_ = comboBox4.Text;
            
            string director = comboBox5.Text;
            
            string pricefrom=comboBox6.Text;
            
            string priceto=comboBox7.Text;
            
            string sorting = comboBox2.Text;

            try
            {
                BigInteger pricefrom_ = BigInteger.Parse(pricefrom);
               
                BigInteger priceto_ = BigInteger.Parse(priceto);
                
                try
                {
                    sort analyse_ = new sort(productname_, typeofproduct_, director, pricefrom, priceto, ID_, sorting);
                   
                    List<sort.Quantity> listquantity = analyse_.InfolistQuantityget;

                    List<sort.Price> listprice = analyse_.Infolistpriceget;

                   
                    if (comboBox1.Text == "Nejčasteji přidané")
                    {
                        List<int> Values_ = new List<int> ();
                        string space = "  ";
                        int line = 1;
                        int y = listquantity.Count;
                        int x = 0;

                        while(x!=y)
                        {
                            Quantity maxQuantity = listquantity.OrderByDescending(q => q.rank_get).FirstOrDefault();

                            // Find the price associated with the maximum quantity
                            Price maxPrice = listprice.FirstOrDefault(p => p.Name_get == maxQuantity.Name_get);

                            string output = Environment.NewLine + Environment.NewLine + line + "." + space + "|" + space +maxQuantity.Name_get + space + "|" + space + "PŘIDÁNO:" + space + maxQuantity.rank_get + "x" + space + "|" + space +"Kvantita:"+space+maxQuantity.Quantity_get+space + "|"+space+"Celková cena:"+space+ maxPrice.Price_get ;

                            Values_.Add (maxQuantity.rank_get);

                            listquantity.Remove(maxQuantity);

                            listBox1.Items.Add(output);

                             

                            line++;
                            x++;
                        }
                        // Vytvoření panelu pro graf
                      
                        int total = Values_.Sum();
                        int x_ = 1;

                        foreach (int value in Values_)
                        {
                            int percent = (int)Math.Round(value * 100.0 / total);
                            string bar = new string('█', percent);
                            listBox1.Items.Add($"{x_+ "  "} ({percent}%) {bar}");
                            x_++;
                        }

                    }



                    else if (comboBox1.Text== "Dle kvantity")
                    {

                        List<string> Values_ = new List<string>();

                        string space = "  ";
                        int line = 1;
                        int y = listquantity.Count;
                        int x = 0;

                        while (x != y)
                        {
                            Quantity maxQuantity = listquantity.OrderByDescending(q =>BigInteger.Parse(q.Quantity_get)).FirstOrDefault();
                        
                            Price maxPrice = listprice.FirstOrDefault(p => p.Name_get == maxQuantity.Name_get);

                            string output = Environment.NewLine + Environment.NewLine + line + "." + space + "|" + space + maxQuantity.Name_get + space + "|" + space + "PŘIDÁNO:" + space + maxQuantity.rank_get + "x" + space + "|" + space + "Kvantita:" + space + maxQuantity.Quantity_get + space + "|" + space + "Celková cena:" + space + maxPrice.Price_get;

                            listquantity.Remove(maxQuantity);

                            listBox1.Items.Add(output);

                            BigInteger one_of_value_ = BigInteger.Parse(maxQuantity.Quantity_get);
                            decimal divided = decimal.Divide((decimal)one_of_value_, 100000000M);
                            string result = $"{divided:F10}".TrimEnd('.');
                           
                            Values_.Add(result);

                            line++;
                            x++;
                        }

                        decimal Total = 0;

                        foreach (var nums in Values_)
                        {
                            Total = decimal.Parse(nums) + Total;
                        }

                        List<decimal> procenta = new List<decimal>();

                        decimal oneprocento = Total / 100;

                        foreach (var nums in Values_)
                        {
                            decimal xx = (decimal.Parse(nums));

                            decimal rslt = xx / oneprocento;

                            procenta.Add(rslt);
                        }

                        int x_ = 1;

                        foreach (decimal percent in procenta)
                        {
                            int percentValue = (int)Math.Round((percent / 100) * 100);
                            string bar = new string('█', percentValue);
                            listBox1.Items.Add($"{x_} ({percentValue}%) {bar}");
                            x_++;
                        }


                    }
                    else if (comboBox1.Text== "Dle ceny")
                    {


                        List<string> Values_ = new List<string>();
                       
                        string space = "  ";
                        int line = 1;
                        int y = listquantity.Count;
                        int x = 0;

                        while (x != y)
                        {
                            Price maxPrice = listprice.OrderByDescending(q => BigInteger.Parse(q.Price_get)).FirstOrDefault();

                            Quantity Quantity_ifno = listquantity.FirstOrDefault(p => p.Name_get == maxPrice.Name_get);

                            string output = Environment.NewLine + Environment.NewLine + line + "." + space + "|" + space + maxPrice.Name_get + space + "|" + space + "CELKOVÁ CENA:" + space + maxPrice.Price_get + space + "|" + space + "Kvantita:" + space + Quantity_ifno.Quantity_get + space + "|" + space + "PŘIDÁNO:" + space + Quantity_ifno.rank_get;

                            listprice.Remove(maxPrice);

                            listBox1.Items.Add(output);

                            BigInteger one_of_value_ = BigInteger.Parse(maxPrice.Price_get);
                            decimal divided = decimal.Divide((decimal)one_of_value_, 10000M);
                            string result = $"{divided:F4}".TrimEnd('.');
                            Values_.Add(result);

                            line++;
                            x++;
                        }
                            
                           decimal Total = 0;
                        
                        foreach (var nums in Values_)
                        {  
                            Total = decimal.Parse(nums) + Total;
                        }

                        List<decimal> procenta = new List<decimal>();

                        decimal oneprocento = Total / 100;
                      
                        foreach (var nums in Values_)
                        {
                             decimal xx  =(decimal.Parse(nums));
                        
                            decimal rslt = xx / oneprocento;

                            procenta.Add(rslt);                      
                        }

                        int x_ = 1;

                        foreach (decimal percent in procenta)
                        {
                            int percentValue = (int)Math.Round((percent / 100) * 100);
                            string bar = new string('█', percentValue);
                            listBox1.Items.Add($"{x_} ({percentValue}%) {bar}");
                            x_++;
                        }
                    }
                }
                catch(Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            
            }
            catch (OverflowException ex)
            {
               MessageBox.Show("Došlo k přetečení datového typu : {0}", ex.Message);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Unvalid");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            x = 1;
            Timeline newForm = new Timeline(ID_);
            newForm.Show();
            this.Close();
        }

        private void Analyseform_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }

        void Openform()
        {
            comboBox2.Items.Add("Product");
            comboBox2.Items.Add("Directory");    // funkce
            comboBox2.Items.Add("Type of Product");
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox1.Items.Add("Nejčasteji přidané");
            comboBox1.Items.Add("Dle kvantity");
            comboBox1.Items.Add("Dle ceny");
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox3.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox4.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox5.DropDownStyle = ComboBoxStyle.DropDownList;

            comboBox6.Items.Add("0");

            comboBox7.Items.Add("0");

            comboBox6.Text = "0";

            comboBox7.Text = "0";

            comboBox3.Items.Add("");

            comboBox4.Items.Add("");

            comboBox5.Items.Add("");


            try
            {

                Work_withdata wrk = new Work_withdata("", "", "", "", ID_, 2, "", "");

                foreach (var ob in wrk.Infrastrukturaobject_get.Productname)
                {
                    comboBox3.Items.Add(ob);
                }

                foreach (var ob in wrk.Infrastrukturaobject_get.TypeofProduct)
                {
                    comboBox4.Items.Add(ob);
                }

                foreach (var ob in wrk.Infrastrukturaobject_get.Director)
                {
                    comboBox5.Items.Add(ob);
                }
            }
            catch
            { 
            }
        }
    }
}
