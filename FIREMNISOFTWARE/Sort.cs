﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Reflection.Emit;
using System.Runtime.Serialization.Formatters;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIREMNISOFTWARE
{
    public class sort
    {

       public List<Price> Infolistpriceget { get; set; }        
       public List<Quantity> InfolistQuantityget { get; set; }

      

        private string Productname_prvt;
        private string TypeOfProduct_prvt;
        private string Diretor_prvt;
        private string PriceFrom_prvt;
        private string PriceTo_prvt;
        private string IDprvt_;
        private string Sorting_prvt;



        public sort(string Productname , string TypeOfProduct,string DDirector , string Pricefrom , string Priceto,string Id,string sorting)
        {


            this.Productname_prvt = Productname;
            this.TypeOfProduct_prvt = TypeOfProduct;
            this.Diretor_prvt = DDirector;
            this.PriceFrom_prvt = Pricefrom;
            this.PriceTo_prvt = Priceto;
            this.IDprvt_ = Id;
            this.Sorting_prvt = sorting;
            Analyse_sort();

        }

        public void Analyse_sort()
        {

            bool ERROR = false;

            int control = 0;

            bool entryStop_=false;

            int if_ = 0;

            int thannumber = -1;

             BigInteger num1=0;
             BigInteger num2=0;


            string jsonString;

            List<string> number_list = new List<string>();        
            // Uložení řetězce do souboru
            string path = @"C:\" + IDprvt_; // cesta k souboru       

            string file_Storage_data = Path.Combine(path, "Storage_database_");

            jsonString = File.ReadAllText(file_Storage_data);

            List<Price> Infolistprice = new List<Price>();

            List<Quantity> InfolistQuantity = new List<Quantity>();

            List<string> Products = new List<string>();

            List<Constructor_forUserData.AddTo_Storage> productInfoList_ = new List<Constructor_forUserData.AddTo_Storage>();

            productInfoList_ = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(jsonString);

            if(PriceFrom_prvt != string.Empty && PriceTo_prvt != string.Empty )
            {
                num1 = BigInteger.Parse(PriceFrom_prvt);
                
                num2 = BigInteger.Parse(PriceTo_prvt);

                if_ = 3;
            }
            else if(PriceTo_prvt != string.Empty)
            {

                if_ = 2;

                thannumber = int.Parse(PriceTo_prvt);      
            }
            else if (PriceFrom_prvt!=string.Empty)
            {


                thannumber = int.Parse(PriceTo_prvt);
                if_ = 1;
              

            }
            else if (PriceFrom_prvt==string.Empty &&PriceTo_prvt==string.Empty)
            {



            }

           if (ERROR==false)
           { 
            
                foreach (var ob in productInfoList_)
                {
                    if (ob.ProductName == Productname_prvt || Productname_prvt == String.Empty)
                    {
                        if (ob.TypeOfProduct == TypeOfProduct_prvt || TypeOfProduct_prvt == String.Empty)
                        {

                            if (ob.Director == Diretor_prvt || Diretor_prvt == String.Empty)
                            {
                                if (if_==3 && BigInteger.Parse(ob.Price)>num1 && BigInteger.Parse(ob.Price)<num2  || if_ == 1 && thannumber <= BigInteger.Parse(ob.Price) || if_ == 2 && thannumber >= BigInteger.Parse(ob.Price) || PriceFrom_prvt == string.Empty && PriceTo_prvt == string.Empty)
                                {
                                    if (entryStop_ == false)
                                    {

                                        if (Sorting_prvt == "Productname")
                                        {
                                            control = 0;
                                            Sorting_prvt = ob.ProductName;
                                        }
                                        else if (Sorting_prvt == "Type of Product")
                                        {
                                            control = 1;
                                            Sorting_prvt = ob.TypeOfProduct;

                                        }
                                        else if (Sorting_prvt == "Directory")
                                        {
                                            control = 2;
                                            Sorting_prvt = ob.Director;
                                        }
                                        else
                                        {
                                            control = 3;
                                            Sorting_prvt = ob.ProductName;
                                        }

                                        entryStop_ = true;
                                    }

                                    else if (entryStop_ == true)
                                    {

                                        if (control == 0)
                                        {
                                            Sorting_prvt = ob.ProductName;
                                        }
                                        if (control == 1)
                                        {
                                            Sorting_prvt = ob.TypeOfProduct;
                                        }
                                        if (control == 2)
                                        {
                                            Sorting_prvt = ob.Director;
                                        }
                                        if (control == 3)
                                        {
                                            Sorting_prvt = ob.ProductName;
                                        }
                                    }

                                    if (Products.Contains(Sorting_prvt) == true)
                                    {

                                        Quantity FindObjekt = InfolistQuantity.FirstOrDefault(o => o.Name_get == Sorting_prvt);

                                        BigInteger number = BigInteger.Parse(FindObjekt.Quantity_get);

                                         BigInteger number2= BigInteger.Parse(ob.Quantity);

                                        BigInteger result = number + number2;

                                        FindObjekt.Quantity_get = Convert.ToString(result);

                                        FindObjekt.rank_get++;

                                        Price FindObjekt2 = Infolistprice.FirstOrDefault(o => o.Name_get == Sorting_prvt);

                                        BigInteger numberPrice = BigInteger.Parse(FindObjekt2.Price_get);

                                        BigInteger numberPrice2 = BigInteger.Parse(ob.finalPrice);

                                        BigInteger result2 = numberPrice + numberPrice2; 

                                        FindObjekt2.Price_get = Convert.ToString(result2);

                                        Infolistpriceget = Infolistprice;

                                        InfolistQuantityget = InfolistQuantity;
                                    }
                                    else
                                    {
                                        Quantity object_quantity = new Quantity(Sorting_prvt, ob.Quantity);

                                        Price object_price = new Price(Sorting_prvt, Convert.ToString(ob.finalPrice));

                                        Infolistprice.Add(object_price);

                                        InfolistQuantity.Add(object_quantity);

                                        object_quantity.rank_get = 1;

                                        Infolistpriceget = Infolistprice;

                                        InfolistQuantityget = InfolistQuantity;

                                        Products.Add(Sorting_prvt);
                                    }
                                }
                            }
                        }
                    }        
                }
           }  
           
        }

        public class Quantity
        {
            private int rankofproduct;
            private string Name_;
            private string Quantity_;


              public int rank_get
              { 
                set { rankofproduct = value; }
                get { return rankofproduct; }
              }
             public string Name_get
             {
                
                get { return Name_; }
             }

            public string Quantity_get
            {
                set { Quantity_=value; }
                get { return Quantity_; }
            }



            public Quantity(string PN , string QN)
            {
                this.Name_ = PN;
                this.Quantity_ = QN;
            }
        }
        public class Price
        {
            private string Name_;
            private string Price_;

            public string Name_get
            {

                get { return Name_; }
            }

            public string Price_get
            {
                set { Price_=value; }
                get { return Price_; }
            }


            public Price (string PN, string PR)
            {
                this.Name_ = PN;
                this.Price_ = PR;
            }
        }

      



    }
}
