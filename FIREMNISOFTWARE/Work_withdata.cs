﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http.Json;
using System.Numerics;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FIREMNISOFTWARE.Constructor_forUserData;
using static FIREMNISOFTWARE.Work_withdata;


namespace FIREMNISOFTWARE
{


    public class Work_withdata
    {
        enum Situation
        {
            LoadingDataFromStorage=1,
            LoadingInfrasructurData=2,
            InfrastructurDataAdd=3,
            AddToStorage=4,
            LoadingSpecifyItem=5,
            ChangeData=6,
            DeleteFromStorage=7
        }
        
        
        public List<Constructor_forUserData.AddTo_Storage> Add_product_storage { get; set; }
        public Constructor_forUserData.Infrastructuredata Infrastrukturaobject_get { get; set; } // pro output pro zobrazeni pridanych dat o infrastrutkur


        private string _Productname;
        private string _TypeofPrdc;
        private string _Director;
        private string _Price;
        private string _Idprvt;
        private string _Quantity;
        private string Identifikator;

        public string Productname;
        public string TypeofPrdc;
        public string Price;
        public string Director;
        public string Quantity;

        public Work_withdata(string Productname, string Typeofproduct, string Director, string Price, string prvt, int Situation_, string Quantity, string Identifikator)
        {
            this._Productname = Productname;
            this._TypeofPrdc = Typeofproduct;
            this._Director = Director;
            this._Price = Price;
            this._Idprvt = prvt;
            this._Quantity = Quantity;
            this.Identifikator = Identifikator;
            Situation situation = (Situation)Situation_;


            switch (situation)
            {
                case Situation.LoadingDataFromStorage:
                    loadingDatafromstorage();
                    situation = 0;
                    break;
                case Situation.LoadingInfrasructurData:
                    Loadinginfrastructur_data();
                    situation = 0;
                    break;
                case Situation.InfrastructurDataAdd:
                    Infrastructur_data();
                    situation = 0;
                    break;
                case Situation.AddToStorage:
                    Addto_storage();
                    situation = 0;
                    break;
                case Situation.LoadingSpecifyItem:
                    Load_specifyItem();
                    situation = 0;
                    break;
                case Situation.ChangeData:
                    ChangeData();
                    situation = 0;
                    break;
                case Situation.DeleteFromStorage:
                    Delete_fromstorage();
                    situation = 0;
                    break;
                default:
                    // kód pro neznámý stav
                    break;
            }

        }
        public void loadingDatafromstorage()
        {

            string path = @"C:\" + _Idprvt;   

            string file_Storage_data = Path.Combine(path, "Storage_database_");

            string file_Storage_items = Path.Combine(path, file_Storage_data);

            if (file_Storage_items != null)
            {
                string reading = File.ReadAllText(file_Storage_items);

                Add_product_storage = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(reading);

              
            }
            else
            {
                MessageBox.Show("Žadne data k dispozici");
            }
        }

        private void Infrastructur_data()
        {
            string jsonString;    // String pro uložení řetězce do souboru

            string path = @"C:\" + _Idprvt; // Cesta k adresatu      

            string Data_Infrastruktur= Path.Combine(path, "InfrastrukturaC.json"); // Cesta k slozce

            if (File.Exists(Data_Infrastruktur))
            {

                jsonString = File.ReadAllText(Data_Infrastruktur);

                Constructor_forUserData.Infrastructuredata Infrastrukturaobject = new Constructor_forUserData.Infrastructuredata();

                Infrastrukturaobject = JsonSerializer.Deserialize<Constructor_forUserData.Infrastructuredata>(jsonString);

                if (Productname != string.Empty)
                {
                    if (Infrastrukturaobject.Productname.Contains(Productname) == false)
                    {
                        Infrastrukturaobject.Productname.Add(Productname);
                    }
                    else
                    {
                        MessageBox.Show("Product name existuje!");
                    }
                }

                if (_TypeofPrdc != string.Empty)
                {
                    if (Infrastrukturaobject.TypeofProduct.Contains(_TypeofPrdc) == false)
                    {
                        Infrastrukturaobject.TypeofProduct.Add(_TypeofPrdc);
                    }
                    else
                    {
                        MessageBox.Show("Type of Product existuje!");
                    }
                }

                if (Director != string.Empty)
                {
                    if (Infrastrukturaobject.Director.Contains(Director) == false)
                    {
                        Infrastrukturaobject.Director.Add(Director);
                    }
                    else
                    {
                        MessageBox.Show("Director existuje!");
                    }
                }

                jsonString = JsonSerializer.Serialize(Infrastrukturaobject);
              
                File.WriteAllText(Data_Infrastruktur, jsonString);
            
            }
            
            else if (!File.Exists(Data_Infrastruktur))
            {
                Constructor_forUserData.Infrastructuredata Infrastrukturaobject = new Constructor_forUserData.Infrastructuredata();
                if (Productname != string.Empty)
                {
                    Infrastrukturaobject.Productname.Add(Productname);
                }
                if (_TypeofPrdc != string.Empty)
                {
                    Infrastrukturaobject.TypeofProduct.Add(_TypeofPrdc);
                }
                if (Director != string.Empty)
                {
                    Infrastrukturaobject.Director.Add(Director);
                }

                if (!File.Exists(Data_Infrastruktur))
                {
                    File.Create(Data_Infrastruktur).Close();
                }

                jsonString = JsonSerializer.Serialize(Infrastrukturaobject);
                
                File.WriteAllText(Data_Infrastruktur, jsonString);
            }
        }

        private void Loadinginfrastructur_data()
        {

            string jsonString;
        
            string path = @"C:\" + _Idprvt;     

            string filePathList = Path.Combine(path, "InfrastrukturaC.json");       

            jsonString = File.ReadAllText(filePathList);

            if (jsonString != null)
            {
                Constructor_forUserData.Infrastructuredata Infrastrukturaobject = new Constructor_forUserData.Infrastructuredata();

                Infrastrukturaobject = JsonSerializer.Deserialize<Constructor_forUserData.Infrastructuredata>(jsonString);

                Infrastrukturaobject_get = Infrastrukturaobject;
            }
        }
        public void Addto_storage()
        {
            string jsonstring;
          
            string path = @"C:\" + _Idprvt;      

            string file_Storage_data = Path.Combine(path, "Storage_database_");


            if (File.Exists(file_Storage_data))
            {


                jsonstring = File.ReadAllText(file_Storage_data);

                List<Constructor_forUserData.AddTo_Storage> storageDataList = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(jsonstring);

                Constructor_forUserData.AddTo_Storage object_Add = new Constructor_forUserData.AddTo_Storage();
                {

                    object_Add.set = DateTime.Now.Millisecond.ToString();
                    object_Add.Sec = DateTime.Now.Second.ToString();
                    object_Add.Minute = DateTime.Now.Minute.ToString();
                    object_Add.Hour = DateTime.Now.Hour.ToString();
                    object_Add.Day = DateTime.Now.Day.ToString();
                    object_Add.Month = DateTime.Now.Month.ToString();
                    object_Add.Year = DateTime.Now.Year.ToString();

                    object_Add.Identifikator = object_Add.Year + object_Add.Day + object_Add.Month + object_Add.Hour + object_Add.Minute + object_Add.Sec + object_Add.set;
                    object_Add.ProductName = _Productname;
                    object_Add.TypeOfProduct = _TypeofPrdc;
                    object_Add.Director = _Director;
                    object_Add.Price = _Price;
                    object_Add.Quantity = _Quantity;

                    BigInteger num = BigInteger.Multiply(BigInteger.Parse(object_Add.Quantity), BigInteger.Parse(object_Add.Price));


                    string num_String = num.ToString();
                    
                    object_Add.finalPrice = num_String;
                }

                storageDataList.Add(object_Add);
               
                jsonstring = JsonSerializer.Serialize(storageDataList);

                File.WriteAllText(file_Storage_data, jsonstring);

            }
            else if (!File.Exists(file_Storage_data))
            {
                File.Create(file_Storage_data).Close();

                Constructor_forUserData.AddTo_Storage object_Add = new Constructor_forUserData.AddTo_Storage();
                {
                    object_Add.set = DateTime.Now.Millisecond.ToString();
                    object_Add.Sec = DateTime.Now.Second.ToString();
                    object_Add.Minute = DateTime.Now.Minute.ToString();
                    object_Add.Hour = DateTime.Now.Hour.ToString();
                    object_Add.Day = DateTime.Now.Day.ToString();
                    object_Add.Month = DateTime.Now.Month.ToString();
                    object_Add.Year = DateTime.Now.Year.ToString();

                    object_Add.Identifikator = object_Add.Year + object_Add.Day + object_Add.Month+ object_Add.Hour + object_Add.Minute + object_Add.Sec + object_Add.set;
                    object_Add.ProductName = _Productname;
                    object_Add.TypeOfProduct = _TypeofPrdc;
                    object_Add.Director = _Director;
                    object_Add.Price = _Price;
                    object_Add.Quantity = _Quantity;

                    BigInteger num = BigInteger.Multiply(BigInteger.Parse(object_Add.Quantity), BigInteger.Parse(object_Add.Price));


                    string num_String = num.ToString();

                    object_Add.finalPrice = num_String;
                }


                List<Constructor_forUserData.AddTo_Storage> storageDataList = new List<AddTo_Storage>();

                storageDataList.Add(object_Add);

                jsonstring = JsonSerializer.Serialize(storageDataList);

                File.WriteAllText(file_Storage_data, jsonstring);

            }
        }
       
        public void ChangeData()
        {


            string jsonstring;
       
          
            string path = @"C:\" + _Idprvt;    
           
            string file_Storage_data = Path.Combine(path, "Storage_database_");

            jsonstring = File.ReadAllText(file_Storage_data);

            List<Constructor_forUserData.AddTo_Storage> obj_list_storage = new List<Constructor_forUserData.AddTo_Storage>();

            obj_list_storage = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(jsonstring);


            Constructor_forUserData.AddTo_Storage OBJECT_ = obj_list_storage.Find(p => p.Identifikator == Identifikator);

            if (OBJECT_ != null)
            {
                OBJECT_.ProductName = _Productname;
                
                OBJECT_.Director = _Director;
                
                OBJECT_.Price = _Price;
                
                OBJECT_.Quantity = _Quantity;

                  BigInteger num = BigInteger.Multiply(BigInteger.Parse(OBJECT_.Quantity), BigInteger.Parse(OBJECT_.Price));

                  string num_String = num.ToString();
                    
                  OBJECT_.finalPrice = num_String;

                  string Change_data = JsonSerializer.Serialize(obj_list_storage);
               
                  File.WriteAllText(file_Storage_data, Change_data);
            }
        }

        private void Load_specifyItem()
        {
            string path = @"C:\" + _Idprvt; // cesta k souboru       

            string file_Storage_data = Path.Combine(path, "Storage_database_");

            string file_Storage_items = Path.Combine(path, file_Storage_data);

            if (file_Storage_items != null)
            {
                string reading = File.ReadAllText(file_Storage_items);

                Add_product_storage = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(reading);

                foreach (var obj in Add_product_storage)
                {
                    if(obj.Identifikator==Identifikator)
                    {
                        Director = obj.Director;
                        Productname=obj.ProductName;
                        TypeofPrdc = obj.TypeOfProduct;
                        Price = obj.Price;
                        Quantity= obj.Quantity;
                    }
                }      
            }
        }
        private void Delete_fromstorage ()
        {
            string jsonstring;
          
            string path = @"C:\" + _Idprvt;      

            string file_Storage_data = Path.Combine(path, "Storage_database_");

            jsonstring = File.ReadAllText(file_Storage_data);

            List<Constructor_forUserData.AddTo_Storage> obj_list_storage = new List<Constructor_forUserData.AddTo_Storage>();

            obj_list_storage = JsonSerializer.Deserialize<List<Constructor_forUserData.AddTo_Storage>>(jsonstring);

            Constructor_forUserData.AddTo_Storage OBJECT_ = obj_list_storage.Find(p => p.Identifikator == Identifikator);

            obj_list_storage.Remove(OBJECT_);

            string Change_data = JsonSerializer.Serialize(obj_list_storage);

            File.WriteAllText(file_Storage_data, Change_data);
        } 
    }
}
