﻿using Microsoft.VisualBasic;
using Microsoft.VisualBasic.Logging;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIREMNISOFTWARE
{
    public partial class Form1 : Form
    {
    

        private int x = 0;
        public Form1()
        {
            InitializeComponent();

        }


        private void button2_Click(object sender, EventArgs e)
        {
            x = 1;
            Create CR = new Create();
            CR.WindowState = FormWindowState.Maximized;
            CR.Show();
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string ID_enter = textBox1.Text;
            
            string Code_enter = textBox2.Text;

            Log_ login = new Log_(ID_enter,Code_enter);
            
            if (login.Callback_rtnr==1)
            {
                Timeline TM = new Timeline(ID_enter.Trim());
                x = 1;
                TM.WindowState = FormWindowState.Maximized;
                TM.Show();  
                this.Close();

            }
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
