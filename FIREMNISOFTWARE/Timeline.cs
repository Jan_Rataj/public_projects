﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FIREMNISOFTWARE.Analyse;
using static System.Net.Mime.MediaTypeNames;

namespace FIREMNISOFTWARE
{
    public partial class Timeline : Form
    {
      
        private string _Id;
        
        private int x  = 0;   // slouzi pro OPEN/CANCEL .. V PRIPADE ZAVRENI TIMELINE PRICINOU OTEVRENI OKNA X=1 A PODMINKA DOLE KTERA ZAVRE APP KDYZ JE X=0 NEPROBEHNE
        
        public Timeline(string Id)
        {
            this._Id = Id;   // PREDAVA SE MEZI VSEMI FORMS PRO PRIKLADANI DAT DANYMU LOGINU

            InitializeComponent();     
        }
      
    
        private void button2_Click(object sender, EventArgs e)
        {
            Analyseform form = new Analyseform(_Id);
            this.Visible = false;
            form.Show();
            x = 1;
        }
        private void button6_Click(object sender, EventArgs e)
        {
            x = 1;
            InfoForManipulate obj_InfoForManipulate = new InfoForManipulate(_Id);
            obj_InfoForManipulate.Show();
            this.Close();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            x = 1;
            AddingItems obj_AddToStorage = new AddingItems(_Id);         
            obj_AddToStorage.Show();
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            x = 1;
            Data_find_delete obj_FindChangeDelete = new Data_find_delete(_Id);
            obj_FindChangeDelete.Show();
            this.Close();
        }    
        private void Timeline_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
