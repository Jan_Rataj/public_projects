﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using static FIREMNISOFTWARE.sort;

namespace FIREMNISOFTWARE
{
    public partial class Data_find_delete : Form
    {

        private string Id;
        private int x = 0;
        private int Control_ = 0;

        public Data_find_delete(string Id)
        {
            this.Id = Id;
            InitializeComponent();
            loading_data();
        }
        private void loading_data()
        {
            listBox1.Items.Clear();

            try
            {
                Work_withdata loading_object = new Work_withdata("", "", "", "", Id, 1, "", "");

                foreach (var obj in loading_object.Add_product_storage)
                {
                    string space = "  "; 

                    string output = obj.ProductName + space + "|" + space +  "Typ:"+ space + obj.TypeOfProduct + space + "|" + space + "Cena za jednotku:" + space + "|" + space + "Kvantita:" +space + obj.Quantity + space + "|" + space +"Hodnota:" +space+ obj.finalPrice + ",-" + space+ "|" + space + "Date:" + space + obj.Year + "-" + obj.Day + "-" + obj.Month + "-" + obj.Hour + "-" + obj.Minute + "-" + obj.Sec + "-" + obj.set;
                   
                    listBox1.Items.Add(output);
                }
            }
            catch
            {
                Control_ = 1;
                
                listBox1.Items.Add("Nedostatečné data pro přidaní produktů" + "  " + DateTime.Now + "    " + "FIREMNÍ SOFTWARE");
            }
        }

       
        private void Data_find_delete_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            x = 1;
            Timeline newForm = new Timeline(Id);
            newForm.Show();
            this.Close();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(Control_!=1)
            { 

                ListBox.SelectedObjectCollection selectedItems = listBox1.SelectedItems;
                string result = "";
                
                 foreach (var item in selectedItems)
                 {
                   result += item.ToString() + ", ";
                 }

                // výpis textu všech vybraných položek
                int dateIndex = result.IndexOf("Date", StringComparison.OrdinalIgnoreCase);

                if (dateIndex >= 0)
                {
                    string datePart = result.Substring(dateIndex + "Date:".Length).Trim();

                    string Identifikator = Regex.Replace(datePart, @"[^\d]+", "");

                    Work_withdata Addtocombox = new Work_withdata("", "", "", "", Id, 5, "", Identifikator);

                    comboBox1.Text = Addtocombox.Productname;
                    comboBox2.Text = Addtocombox.TypeofPrdc;
                    comboBox3.Text = Addtocombox.Director;
                    comboBox4.Text = Addtocombox.Price;
                    comboBox5.Text = Addtocombox.Quantity;
                } 
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Control_ != 1)
            {
                try
                {
                    BigInteger num1 = BigInteger.Parse(comboBox4.Text);

                    BigInteger num2 = BigInteger.Parse(comboBox5.Text);

                    ListBox.SelectedObjectCollection selectedItems = listBox1.SelectedItems;
                    string result = "";

                    foreach (var item in selectedItems)
                    {
                        result += item.ToString() + ", ";
                    }

                    // výpis textu všech vybraných položek
                    int dateIndex = result.IndexOf("Date", StringComparison.OrdinalIgnoreCase);

                    if (dateIndex >= 0)
                    {

                        string datePart = result.Substring(dateIndex + "Date:".Length).Trim();

                        string Identifikator = Regex.Replace(datePart, @"[^\d]+", "");

                        Work_withdata changedata = new Work_withdata(comboBox1.Text.Trim(), comboBox2.Text.Trim(), comboBox3.Text.Trim(), comboBox4.Text.Trim(), Id, 6, comboBox5.Text.Trim(), Identifikator);

                        loading_data();
                    }
                }
                catch
                {
                    MessageBox.Show("UNVALID");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Control_ != 1)
            {

                ListBox.SelectedObjectCollection selectedItems = listBox1.SelectedItems;  //oznaceny pole

                string result = "";

                foreach (var item in selectedItems)
                {
                    result += item.ToString() + ", ";
                }

                int dateIndex = result.IndexOf("Date", StringComparison.OrdinalIgnoreCase);

                if (dateIndex >= 0)
                {
                    string datePart = result.Substring(dateIndex + "Date:".Length).Trim();

                    string Identifikator = Regex.Replace(datePart, @"[^\d]+", "");

                    Work_withdata Deleteinstorage = new Work_withdata("", "", "", "", Id, 7, "", Identifikator);


                    loading_data();
                }
            }
        }

        private void Data_find_delete_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (x != 1)
            {
                x = 0;
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}
